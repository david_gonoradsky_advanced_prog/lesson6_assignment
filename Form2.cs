﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using System.Globalization;

namespace WindowsFormsApplication1
{
    public partial class Form2 : Form
    {
        Dictionary<string, string> details = new Dictionary<string, string>();
        string _user;
        public Form2(string user)
        {
            InitializeComponent();
            _user = user;
        }
        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            bool isBD = false;
            string str;
            string path = "C:\\Users\\User\\Documents\\Study\\Magshimim\\2016-2017\\Advanced Programing\\Semester_B\\DotNet\\" + _user + "BD.txt";
            using (StreamReader sr = new StreamReader(path))
            {
                if ((str = sr.ReadToEnd()) != null)
                {
                    string selectedDate = monthCalendar1.SelectionRange.Start.Date.ToShortDateString();
                    string[] words = str.Split(',', '\n', '\r');
                    /*for (int i = 0; i < words.Length; i += 2)
                    {
                        details.Add(words[i], words[i + 1]);
                    }*/
                    for (int i = 1; i < words.Length; i += 3)
                    {
                        if (words[i].CompareTo(selectedDate) == 0)
                        {
                            isBD = true;
                            MessageBox.Show("On this date - " + words[i-1] + " has birthday");
                        }
                    }
                    if(!isBD)
                    {
                        MessageBox.Show("On this date - no one from youe list has birthday");
                    }
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
