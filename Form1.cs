﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            bool isLog = false;
            //Dictionary<string, string> details = new Dictionary<string, string>();
            string str;
            using (StreamReader sr = new StreamReader("C:\\Users\\User\\Documents\\Study\\Magshimim\\2016-2017\\Advanced Programing\\Semester_B\\DotNet\\Users.txt"))
            {
                if((str = sr.ReadToEnd()) != null)
                {
                    string[] words = str.Split(',', '\n', '\r');
                   /* for(int i = 0; i < words.Length; i+=2)
                    {
                        details.Add(words[i], words[i+1]);                    
                    }*/
                    for (int i = 0; i < words.Length - 1; i += 3)
                    {
                        if (words[i].CompareTo(this.txtUser.Text) == 0 && words[i+1].CompareTo(this.txtPass.Text) == 0)
                        {
                            isLog = true;
                            Form2 cln = new Form2(this.txtUser.Text);
                            this.Hide();
                            cln.ShowDialog();
                            this.Show();
                            this.Close();
                        }
                    }
                }
                if(!isLog)
                {
                    MessageBox.Show("Worng username or password!");
                    txtPass.Text = "";
                }
            }
        }
    }
}
